class MonsterResponse {
  num id;
  String imageUrl;
  String name;
  String type;
  String character;
  String classify;
  String explain;
  num monHp;
  num monPower;
  num monDefensive;
  num monSpeed;

  MonsterResponse(this.id, this.imageUrl, this.name, this.type, this.character, this.classify,this.explain, this.monHp, this.monPower, this.monDefensive, this.monSpeed);

  factory MonsterResponse.fromJson(Map<String, dynamic>json) {
    return MonsterResponse(
      json['id'],
      json['imageUrl'],
      json['name'],
      json['type'],
      json['character'],
      json['classify'],
      json['explain'],
      json['monHp'],
      json['monPower'],
      json['monDefensive'],
      json['monSpeed'],
    );
  }
}