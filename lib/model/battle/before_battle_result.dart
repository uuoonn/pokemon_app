import 'package:pokemon_app/model/battle/monster_info_in_battle.dart';
import 'package:pokemon_app/model/battle/monsters_in_stage.dart';

class BeforeBattleResult {
  MonsterInStage data;

  BeforeBattleResult(this.data);

  factory BeforeBattleResult.fromJson(Map<String, dynamic> json) {
    return BeforeBattleResult(
        MonsterInStage.fromJson(json['data'])
    );
  }
}