class MonsterInfoInBattle {

  String imgUrl;
  String name;
  num monHp;
  num monPower;
  num monDefensive;
  num monSpeed;

  MonsterInfoInBattle(
      this.imgUrl,
      this.name,
      this.monHp,
      this.monPower,
      this.monDefensive,
      this.monSpeed
      );

  factory MonsterInfoInBattle.fromJson(Map<String, dynamic>json) {
    return MonsterInfoInBattle(
        json['imgUrl'],
        json['name'],
        json['monHp'],
        json['monPower'],
        json['monDefensive'],
        json['monSpeed'],
    );
  }
}