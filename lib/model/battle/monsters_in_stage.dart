import 'package:pokemon_app/model/battle/battle_monster_item.dart';

class MonsterInStage {
  BattleMonsterItem monster1;
  BattleMonsterItem monster2;

  MonsterInStage(this.monster1, this.monster2) ;

  factory MonsterInStage.fromJson(Map<String, dynamic> json) {
    return MonsterInStage(
      BattleMonsterItem.fromJson(json['monster1']),
        BattleMonsterItem.fromJson(json['monster2'])
    );
  }



}