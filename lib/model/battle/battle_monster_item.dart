class BattleMonsterItem {
  num id;
  num monsterId;
  String name;
  String imgUrl;
  num monHp;
  num monPower;
  num monDefensive;
  num monSpeed;

  BattleMonsterItem (
      this.id,
      this.monsterId,
      this.name,
      this.imgUrl,
      this.monHp,
      this.monDefensive,
      this.monPower,
      this.monSpeed,
      );

  factory BattleMonsterItem.fromJson(Map<String, dynamic> json) {
    return BattleMonsterItem (
      json['id'],
      json['monsterId'],
      json['name'],
      json['imgUrl'],
      json['monHp'],
      json['monDefensive'],
      json['monPower'],
      json['monSpeed'],
    );
  }



}