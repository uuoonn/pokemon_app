import 'package:dio/dio.dart';
import 'package:pokemon_app/config/config_api.dart';
import 'package:pokemon_app/model/battle/before_battle_result.dart';

import '../model/common_result.dart';

class RepoFightStage {
  Future<BeforeBattleResult> getCurrentState() async {
    Dio dio = Dio();
    String _baseUrl = '$apiUri/v1/battle/in-battle-stage';

    final response = await dio.get(_baseUrl,
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        })
    );
    return BeforeBattleResult.fromJson(response.data);

  }

  Future<CommonResult> setStageIn(num pokemonId) async {
    Dio dio = Dio();
    String _baseUrl = '$apiUri/v1/battle/join-id/{pokemonId}';

    final response = await dio.post(
        _baseUrl.replaceAll('{pokemonId}', pokemonId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            })
    );
    
    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> delStageOut(num battleId) async {
    Dio dio = Dio();
    String _baseUrl = '$apiUri/v1/battle/del/{battleId}';

    final response = await dio.delete(
        _baseUrl.replaceAll('{battleId}', battleId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            })
    ) ;
    return CommonResult.fromJson(response.data);


    
  }

}