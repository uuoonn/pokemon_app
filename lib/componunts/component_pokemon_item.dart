import 'package:flutter/cupertino.dart';
import 'package:pokemon_app/model/monster/pokemon-item.dart';

class ComponentPokemonItem extends StatelessWidget {
  const ComponentPokemonItem({
    super.key,
    required this.pokemonItem,
    required this.callback,

  });

  final PokemonItem pokemonItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              child: SizedBox(
                width: 200,
                height: 200,
                child: Image.asset(
                  pokemonItem.imageUrl,
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Container(
              child: Column(
                children: [
                  Text(
                    pokemonItem.name,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  Text(
                    pokemonItem.type
                  ),
                  Text(
                    pokemonItem.character
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
