import 'package:flutter/material.dart';
import 'package:pokemon_app/model/battle/battle_monster_item.dart';


class ComponentBattleMonster extends StatefulWidget {
  const ComponentBattleMonster({
    super.key,
    required this.battleMonsterItem,
    required this.callback,
    required this.isMyTurn,
    required this.isLive,
    required this.currentHp,
  });

  final BattleMonsterItem battleMonsterItem;
  final VoidCallback callback;
  final bool isMyTurn;
  final bool isLive;
  final num currentHp;

  @override
  State<ComponentBattleMonster> createState() => _ComponentBattleMonsterState();
}

class _ComponentBattleMonsterState extends State<ComponentBattleMonster> {

  double _calculateHpPercent() {
    return widget.currentHp / widget.battleMonsterItem.monHp;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width /3,
            height: MediaQuery.of(context).size.width / 3,
            child: Image.asset(
              '${widget.isLive ? widget.battleMonsterItem.imgUrl : 'assets/out.png'}',
              fit: BoxFit.fill,
            ),
          ),
          Text(widget.battleMonsterItem.name),
          Text('총 HP ${widget.battleMonsterItem.monHp}'),
          Text('총 HP ${widget.battleMonsterItem.monPower}'),
          Text('총 HP ${widget.battleMonsterItem.monDefensive}'),
          Text('총 HP ${widget.battleMonsterItem.monSpeed}'),
          SizedBox(
            width: MediaQuery.of(context).size.width / 2,
            child: LinearProgressIndicator(
              value: _calculateHpPercent(),
            ),
          ),
          (widget.isMyTurn && widget.isLive) ?
              OutlinedButton(
                onPressed: widget.callback,
                child: Text('공격'),
              ) : Container(),
        ],
      ),
    );
  }
}
