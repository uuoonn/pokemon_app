import 'package:flutter/material.dart';
import 'package:pokemon_app/componunts/component_battle_monster.dart';
import 'package:pokemon_app/model/battle/monster_info_in_battle.dart';

import '../../model/battle/battle_monster_item.dart';

class PageBattleStage extends StatefulWidget {
  const PageBattleStage({
    super.key,
    required this.monster1,
    required this.monster2
  });


  final BattleMonsterItem monster1;
  final BattleMonsterItem monster2;

  @override
  State<PageBattleStage> createState() => _PageBattleStageState();
}

class _PageBattleStageState extends State<PageBattleStage> {

  //기본으로 1번 몬스터가 먼저 공격한다. 1변 몬스터 순서면 2번 몬스터는 공격할 수 없다.
  bool _isTurnMonster1 = true;

  //몬스터 1이 살아있나 체크, 현재 HP 체크 = 기본값 0
  bool _monster1Live = true;
  num _monster1CurrentHp = 0;

  //몬스터 2이 살아있나 체크, 현재 HP 체크 = 기본값 0
  bool _monster2Live = true;
  num _monster2CurrentHp = 0;


  String _gameLog = '';

  get monster1 => null;

  @override
  void initState() {
    super.initState();
    _calculateFirstTurn(); // 몬스터 선공격 판별
    setState(() {
      _monster1CurrentHp = widget.monster1.monHp; // 1번 몬스터 잔여 hp (페이지 들어어자마자니까 최대체력만큼)
      _monster2CurrentHp = widget.monster2.monHp; // 2번 몬스터 잔여 hp (페이지 들어어자마자니까 최대체력만큼)
    });
  }

  // 선공격 누구인 지 계산하기
  void _calculateFirstTurn() {
    if (widget.monster1.monSpeed < widget.monster2.monSpeed) { //몬스터 1보다 몬스터 2의 스피드가 더 빠를 경우,
      setState(() {
        _isTurnMonster1 =false; // 2번이 선공격
      });
    }
  }

  //때렸을 때 최종 공격력 몇인지 (크리티컬 데미지 몇인지)
  num _calculateResultHitPoint(num myHitPower, num targetDefensive) {
    List<num> criticalArr = [1,2,3,4,5]; // 20프로 확률로 크리티컬 데미지
    criticalArr.shuffle();

    bool isCritical = false; // 기본으로 크리티컬 안터진다.
    if (criticalArr[0] == 1) isCritical = true; // 다 섞은 criticalArr에 0번째 요소가 1이면 크리뜬걸로..

    num resultHit = myHitPower; //공격력
    if (isCritical) resultHit = resultHit * 2; //크리티컬이면 공격력 2배
    resultHit = resultHit - targetDefensive; // 상대방 방어력만큼 데미지 깎기
    resultHit = resultHit.round(); // 반올림 처리

    if(resultHit <= 0) resultHit = 1; // 계산 결과 공격이 0이거나 음수일 경우 공격포인트 고정 1 처리

    return resultHit;
  }


  //상대 몬스터가 죽었는 지 확인하기
  void _checkDead(num targetMonster) {
    if (targetMonster == 1 && (_monster1CurrentHp <= 0)) { //몬스가 1번일 경우, + 몬스터의 현재 HP가 0또는 0보다 작을 경우
      setState(() {
        _monster1Live = false;
      });
    } else if (targetMonster == 2 && (_monster2CurrentHp <= 0)) {
      setState(() {
        _monster2Live = false;
      });
    }
  }

  // 공격처리
  void _attMonster(num actionMonster)  {
    num myHitPower = widget.monster1.monPower;
    num targetDefensive = widget.monster2.monDefensive;

    if (actionMonster == 2) {
      myHitPower = widget.monster2.monPower;
      targetDefensive = widget.monster1.monDefensive;
    }

    num resultHit = _calculateResultHitPoint((myHitPower), targetDefensive) ;

    if (actionMonster == 1) { //공격하는 몬스터가 1번이면
      setState(() {
        _monster2CurrentHp -= resultHit; // 2번 몬스터 체력 깎기
        if (_monster2CurrentHp <= 0) _monster2CurrentHp =0; // 체력 0 미만이면 0으로 고정
        _checkDead(2); // 2번 몬스터 죽었는 지 확인
      });
    } else{
      setState(() {
        _monster1CurrentHp -= resultHit; // 1번 몬스터 체력 깎기
        if (_monster1CurrentHp <= 0) _monster1CurrentHp - 0; //체력 0 미만이면 0으로 고정
        _checkDead(1);
      });
    }
    setState(() {
      _isTurnMonster1 = !_isTurnMonster1; //턴 넘기기 not연산자로 계속 바꿔주기.
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('배틀을 해보자'),
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Row(
            children: [
              ComponentBattleMonster(
                battleMonsterItem: widget.monster1,
                  callback: () {
                    _attMonster(1);
                  },
                  isMyTurn: _isTurnMonster1,
                  isLive: _monster1Live,
                  currentHp: _monster1CurrentHp,
              ),
              ComponentBattleMonster(
                battleMonsterItem: widget.monster2,
                  callback: () {
                    _attMonster(2);
                  },
                  isMyTurn: ! _isTurnMonster1,
                  isLive: _monster2Live,
                  currentHp: _monster2CurrentHp,
              )
            ],
          ),
          Text(_gameLog),
        ],
      ),
    );
  }
}
