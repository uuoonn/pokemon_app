import 'package:flutter/material.dart';
import 'package:pokemon_app/model/battle/battle_monster_item.dart';
import 'package:pokemon_app/pages/battle/page_battle_stage.dart';
import 'package:pokemon_app/repository/repo_fight_stage.dart';

class PageReadyBattle extends StatefulWidget {
  const PageReadyBattle({super.key});

  @override
  State<PageReadyBattle> createState() => _PageReadyBattleState();
}

class _PageReadyBattleState extends State<PageReadyBattle> {
  BattleMonsterItem? monster1;
  BattleMonsterItem? monster2;

  //현재 결투장에 진입중인 몬스터들 정보 가져오기
  Future<void> _loadDetail() async {
    await RepoFightStage().getCurrentState()
        .then((res) => {
          setState((){
            monster1 = res.data.monster1;
            monster2 = res.data.monster2;
          })
    });
  }

  //결투장에서 몬스터 퇴출시키기
  Future<void> _delMonster(num battleId) async {
    await RepoFightStage().delStageOut(battleId)
        .then((res) => {
          _loadDetail()
    });
  }

  //페이지 생성되고 화면에 부착하기전에 데이터부터 쥐어줘야함.
  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('전투 대기실'),
        centerTitle: true,
      ),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          monster1 != null ?
          Container(
            child: Column(
              children: [
                Text(monster1!.name),
                OutlinedButton(
                    onPressed: () {
                      _delMonster(monster1!.id);
                    },
                    child: Text ('퇴장'),
                ),
              ],
            ),
          )  : Container(),
          monster2 != null ?
          Container(
            child: Column(
              children: [
                Text(monster2!.name),
                OutlinedButton(
                    onPressed: (){
                      _delMonster(monster2!.id);
                    },
                    child: Text('퇴장'),
                ),
              ],
            ),
          )    : Container(),

          monster1 != null && monster2 != null ?
          OutlinedButton(
              onPressed: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageBattleStage(monster1: monster1!, monster2: monster2!)));
          },
              child: Text('배틀시작'),
          ) : Container(),
        ],
      ),
    );

  }
}
