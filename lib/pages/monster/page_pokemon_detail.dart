import 'package:flutter/material.dart';
import 'package:pokemon_app/model/monster/monster_response.dart';
import 'package:pokemon_app/model/monster/pokemon-item.dart';
import 'package:pokemon_app/pages/battle/page_battle_stage.dart';
import 'package:pokemon_app/pages/battle/page_ready_battle.dart';

class PagePokemonDetail extends StatefulWidget {
  const PagePokemonDetail({
    super.key,
    required this.monsterResponse,
  });

  final MonsterResponse monsterResponse;


  @override
  State<PagePokemonDetail> createState() => _PagePokemonDetailState();
}

class _PagePokemonDetailState extends State<PagePokemonDetail> {



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlueAccent,
        centerTitle: true,
        title: Text(
            '포켓몬 상세보기',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: _buildBody(context),
    );
  }
  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        Container(
          child: Column(
            children: [
              Text('${widget.monsterResponse.id}'),
              Image.asset(widget.monsterResponse.imageUrl),
              Text(widget.monsterResponse.name),
              Text(widget.monsterResponse.character),
              Text(widget.monsterResponse.classify),
              Text(widget.monsterResponse.explain),
              Text('HP:${widget.monsterResponse.monHp}'),
              Text('POWER:${widget.monsterResponse.monPower}'),
              Text('DEFENSIVE:${widget.monsterResponse.monDefensive}'),
              Text('SPEED:${widget.monsterResponse.monSpeed}'),
              OutlinedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageReadyBattle()));

                  },
                  child:Text('배틀 참가하기') )
            ],
          ),
        ),
      ],
    );

  }
}
