import 'package:flutter/material.dart';
import 'package:pokemon_app/componunts/component_pokemon_item.dart';
import 'package:pokemon_app/model/monster/monster_response.dart';
import 'package:pokemon_app/model/monster/pokemon-item.dart';
import 'package:pokemon_app/pages/monster/page_pokemon_detail.dart';

class PagePokemonList extends StatefulWidget {
  const PagePokemonList({
    super.key,
    required this.pokemonItem,
    required this.monsterResponse,
  });


  final PokemonItem pokemonItem;
  final MonsterResponse monsterResponse;

  @override
  State<PagePokemonList> createState() => _PagePokemonListState();
}

class _PagePokemonListState extends State<PagePokemonList> {
  List<PokemonItem> _list = [];

  List<MonsterResponse> _response =[];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlueAccent,
        centerTitle: true,
        title: const Text(
            '포켓몬스터',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: GridView.builder(
          itemCount: _list.length,
          itemBuilder: (BuildContext context, int idx) {
            return ComponentPokemonItem(
                pokemonItem: _list[idx],
                callback: (){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => PagePokemonDetail(monsterResponse: _response[idx])));
                }
            );
          },
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2

      ),
      ),

    );
  }
}
